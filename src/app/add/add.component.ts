import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Post } from '../shared/post.modal';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  title = '';
  description = '';
  date = '';
  post!: Post;
  postId = '';



  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId = params['id'];
      this.http.get<Post>(`https://alina-beaf9-default-rtdb.firebaseio.com/posts/${postId}.json`)
        .subscribe(result => {
          this.post = result;
          this.date = this.post.date;
          this.title = this.post.title;
          this.description = this.post.description;
        })
    });

  }

  OnEdit() {
    if (this.postId){
      const date = this.date;
      const title = this.title;
      const description = this.description;
      const body = {date, description, title};
      this.http.put<Post>(`https://alina-beaf9-default-rtdb.firebaseio.com/posts/${this.postId}.json`, body).subscribe();
    } else {
      const date = this.date;
      const title = this.title;
      const description = this.description;
      const body = {date, title, description};
      this.http.post('https://alina-beaf9-default-rtdb.firebaseio.com/posts.json', body).subscribe();
    }
  }
}
