import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddComponent } from './add/add.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { PostDetailsComponent } from './posts/post-details/post-details.component';

const routes: Routes = [
  {path: 'posts', component: HomeComponent},
  {path: 'posts/:id', component: PostDetailsComponent},
  {path: 'posts/:id/edit', component: AddComponent},
  {path: 'add', component: AddComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contacts', component: ContactsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
