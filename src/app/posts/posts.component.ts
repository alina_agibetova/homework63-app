import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Post } from '../shared/post.modal';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts!: Post[];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get<{[id: string]: Post}>('https://alina-beaf9-default-rtdb.firebaseio.com/posts.json')
      .pipe(map(result => {
        if (result === null){
          return [];
        }
        return Object.keys(result).map(id => {
          const postData = result[id];

          return new Post(id, postData.date, postData.description, postData.title);
        });
      }))

      .subscribe(posts => {
        this.posts = posts;
      });
  }

}
