import { Component, OnInit } from '@angular/core';
import { Post } from '../../shared/post.modal';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post!: Post;


  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId = params['id'];
      this.http.get<Post>(`https://alina-beaf9-default-rtdb.firebaseio.com/posts/${postId}.json`)
        .pipe(map(result => {
          this.post = result;
        }))
        .subscribe();
    });
  };

  OnDelete(){
    void this.router.navigate(['posts']);
    this.route.params.subscribe((params: Params) => {
      const postId = params['id'];
      console.log(postId);
      this.http.delete(`https://alina-beaf9-default-rtdb.firebaseio.com/posts/${postId}.json`).subscribe();
    });
  }


}
