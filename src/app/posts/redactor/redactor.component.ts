import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Post } from '../../shared/post.modal';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-redactor',
  templateUrl: './redactor.component.html',
  styleUrls: ['./redactor.component.css']
})
export class RedactorComponent implements OnInit {
  post: Post | null = null;


  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId = params['id'];
      this.http.put('https://alina-beaf9-default-rtdb.firebaseio.com/posts/post1.json', postId)

    });
  }
}
